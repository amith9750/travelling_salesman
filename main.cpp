#include <iostream>
#include <vector>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <fstream>
using namespace std;
class Cities
{
    public:
        int id;
        int x;
        int y;
        vector <Cities*> neighbours; //positions
        Cities(int idin, int xin, int yin)
        {
            id = idin;
            x = xin;
            y = yin;
        }
        Cities(Cities & source)
        {
            x = source.x;
            y = source.y;
            id = source.id;
            neighbours = source.neighbours;
        }

        int dist(Cities * Cities_in) //calculates the Euclidean distance to another Cities
        {
            float x2 = Cities_in->x;
            float y2 = Cities_in->y;

            x2 -= x;
            y2 -= y;

            x2 *= x2;
            y2 *= y2;

            return floor(sqrt(x2 + y2) + 0.5);  //Rounds down from 0.0 to .499_, rounds up from 0.5 to .99_
        }
};

class Travel_Man
{
    public:
        vector <Cities*> initial; //Stores the initial list of cities
        vector <Cities*> current; //Stores the current solution
        int num_cities; //Stores the number of cities read into original_list
        Travel_Man(const char * filename) //Constructor, takes filename to read from as input
        {
            num_cities = read_file(filename);
        }
        Travel_Man(Travel_Man & source)
        {
            int size = source.initial.size();
            for (int i = 0; i < size; ++i) {
                initial.push_back(new Cities(*source.initial[i]));
            }

            size = source.current.size();

            for (int i = 0; i < size; ++i) {
                current.push_back(new Cities(*source.current[i]));
            }

            num_cities = source.num_cities;
        }
        int read_file(const char * filename) //Reads a list of cities into original_list from filename
        {
            int no_of_cities = 0;
            int id_read = 0;
            int x_read = 0;
            int y_read = 0;

            initial.clear();
            ifstream read(filename); //open file

            if (!read)
            {
                cout << "FILE DOES NOT EXIST :(" << endl;
                return 0; //file doesn't exist
            }

            while (read>>id_read>>x_read>>y_read) //go till end of file
            {
                initial.push_back(new Cities(id_read, x_read, y_read));
                ++no_of_cities;
            }

            read.close();
            return no_of_cities;
        }
        void write_solution() //Writes a solution to file_name
        {
            int distance = distance_between();
                cout  << "Distance Travelled = " << distance << '\n';
            cout << "Order of travelling cities :" << endl;
            for (int i = 0; i < num_cities; ++i)
                cout << current[i]->id << endl;
            return;
        }

        int solve_tsp() //Nearest neighbor with 2-opt (calls solve_tsp_util() and two_change())
        {
            int tour_total_dist = 0;
            int best_start_distance = INT_MAX;
            int last_run = 0;

            //Run through entire 2-opt optimization for each city, write best index.
            for (int i = 0; i < num_cities; ++i)
            {
                solve_tsp_util(i);
                last_run = two_change();
                if (last_run < best_start_distance)
                {
                    best_start_distance = last_run;
                    cout << "SOLUTION :" << endl;
                    write_solution();  //write each time an improvement is found
                    cout << endl;
                }
            }


            tour_total_dist = distance_between();
            if (best_start_distance <= tour_total_dist)
                return best_start_distance;     //soluti8on already written

            else
            {
                cout << "SOLUTION :"<< endl;
                write_solution();       //write current solution (midway through 2-opt)
                cout << endl;
                return best_start_distance;
            }
        }
        int solve_tsp_util(int start_index) //Generates basic nearest neighbor tour beginning at start_index
        {
            int total_cities = 0;
            int closest = INT_MAX;
            int tour_total_dist = 0;
            int current_dist = 0;
            int closest_index = 0;
            int remain_cities = num_cities;
            vector <Cities*> temp(initial);
            current.clear();
            current.push_back(initial[start_index]);     //move initial city to solution
            initial.erase(initial.begin() + start_index);       //erase from original_list
            --remain_cities;        //cities remaining in original_list
            ++total_cities;       //number of cities in solution so far
            while(remain_cities != 0)             //loop until no cities remaining in original_list
            {
                closest = INT_MAX;  //reset closest to a large number so that comparison will work
                for (int i = 0; i < remain_cities; ++i)
                {
                    current_dist = initial[i]->dist(current[total_cities-1]);
                    if (current_dist < closest)
                    {
                        closest_index = i;
                        closest = current_dist;
                    }
                }

                tour_total_dist += closest;
                current.push_back(initial[closest_index]);
                initial.erase(initial.begin() + closest_index);

                --remain_cities;
                ++total_cities;
                }

            initial = temp;
            return tour_total_dist + current[0]->dist(current[total_cities-1]);
        }
        int two_change() //Semi-naive 2-opt implementation.
        {
            vector <Cities*> new_path;
            int min_distance = distance_between();
            bool start_over = false;
            while(start_over)
            {
                start_over = false;
                for (int i = 1; i < num_cities && !start_over; ++i)
                {
                    for (int j = i+1; j < num_cities-1 && !start_over; ++j)
                    {
                        //only check moves that will reduce distance
                        if (current[i-1]->dist(current[j]) + current[i]->dist(current[j+1]) < current[i-1]->dist(current[i]) + current[j]->dist(current[j+1]))
                        {
                            swap_two(i, j);
                            min_distance = distance_between();
                            start_over = true;
                        }

                        else
                            start_over = false;
                    }
                 }
            }
            return min_distance;
        }
        int swap_two(int i, int k) //Used by two_opt()
        {
            vector <Cities*> temp;
            int count = 0;

            //Reverse order
            for (int x = k; x >= i; --x)
            {
                temp.push_back(current[x]);
            }

            for (int x = i; x <= k; ++x)
            {
                current[x] = temp[count];
                ++count;
            }
            temp.clear();
            return 1;
        }
        int distance_between() //Finds total distance of tour in solution
        {
            int tour_total_dist = 0;
            for (int i = 0; i < num_cities - 1; ++i)
            {
                tour_total_dist += current[i]->dist(current[i+1]);
            }

            tour_total_dist += current[0]->dist(current[num_cities-1]);
            return tour_total_dist;
        }
};

int main()
{
    clock_t t = clock();
    Travel_Man TSP("example_input.txt");
    TSP.solve_tsp();
    cout << "Runtime : " << (double)(clock() - t )/CLOCKS_PER_SEC << " sec" << endl;
    return 0;
}
